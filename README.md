React-Diagnoses
===============
Testing React JS with searching large data....

Warning: it contains lot of data - it may hang/crash you browser...

Live demos
----------

*  [Live demo](http://hpaluch.bitbucket.org/react-diag-indexed-db/)
of React JS Diagnoses search running with Indexed DB. This version is on `indexed_db` branch.



On CentOS 6.x
=============

Enable EPEL:

	rpm -ivh http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm

Install grunt:

	yum install nodejs-grunt-cli nodejs-grunt-init npm


On Ubuntu 14
============

Please follow step-by-step manual on http://www.saltycrane.com/blog/2014/11/how-install-grunt-ubuntu-1404/ written by Mr. Eliot. 


Common setup
============

Setup this project:

	git clone https://bitbucket.org/hpaluch/react-diag.git
	cd react-diag/
	npm install

Build (inside of project dir):

	grunt

Install:

	cp build/* /your_html_root_dir

Use:

Point your browser to location of copied files (something like http://localhost/test/index.html) - you should see first 100 rows. 
Enter diagnose code or name into search field for example 'h05' or 'deformace'...


Resources
=========

* http://facebook.github.io/react/docs/thinking-in-react.html
* http://simonsmith.io/writing-react-components-as-commonjs-modules/ (nice but the react transform does not work for me)






