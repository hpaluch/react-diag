// Most of code based on: http://facebook.github.io/react/docs/thinking-in-react.html
// and some on http://simonsmith.io/writing-react-components-as-commonjs-modules/
// use react as module (browserify will do the job)
var React = require('react');

var DiagRow = React.createClass({
    render: function() {
        return (
            <tr>
                <td>{this.props.diag.id}</td>
                <td>{this.props.diag.name}</td>
            </tr>
        );
    }
});

var DiagTable = React.createClass({
    render: function() {
        var rows = [];
        var lc = this.props.filterText.toLowerCase();
        this.props.diags.forEach(function(diag) {
            if ( rows.length<=100){
                if (diag.id.toLowerCase().indexOf(lc) === -1 
                    && diag.name.toLowerCase().indexOf(lc) === -1) {
                   return;
                }
               rows.push(<DiagRow diag={diag} key={diag.id} />);
            }
        }.bind(this));
        return (
            <table>
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});

var SearchBar = React.createClass({
    handleChange: function() {
        this.props.onUserInput(
            this.refs.filterTextInput.getDOMNode().value
        );
    },
    render: function() {
        return (
            <form>
                <input type="text" placeholder="Search..."
                       value={this.props.filterText}
                       ref="filterTextInput"
                       onChange={this.handleChange} />
            </form>
        );
    }
});

var FilterableDiagTable = React.createClass({
    getInitialState: function() {
        return {
            filterText: ''
        };
    },
    handleUserInput: function(filterText) {
        this.setState({
            filterText: filterText
        });
    },
    render: function() {
        return (
            <div>
                <h1>Search diagnoses</h1>
                <p>Loaded total { this.props.data.length } diagnoses from data.js</p>
                <SearchBar filterText={this.state.filterText}
                           onUserInput={this.handleUserInput} />
                <DiagTable diags={this.props.data}
                           filterText={this.state.filterText} />
            </div>
        );
    }
});


// diagData array declared in data.js
React.render(
  <FilterableDiagTable data={diagData} />,
  document.getElementById('content')
);

