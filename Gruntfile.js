module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    copy: {
      staticfiles: {
           expand: true,
           dest: 'build/',
           cwd: 'src',
           src: [ '*.html' ] 
      },
      datafile: {
           expand: true,
           dest: 'build/',
           cwd: 'data',
	   // cwd:  'data-small',   // for testing/development
           src: [ '*.js' ]
      }
    },
    browserify: {
      client: {
              files: { 'build/combined.js': [ 'src/DiagTable.jsx' ] },
	      options: {
		transform: [  'reactify'  ]
	      }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browserify');

  // Default task(s).
  grunt.registerTask('default', ['browserify','copy']);
};
