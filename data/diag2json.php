<?php

function nn($str){
   return strlen(trim($str))>0 ? $str  : "";
}

$MY_COLS = 6;
$row = 1;

$JSON = array();

if (($handle = fopen("php://stdin", "r")) !== FALSE) {

    while (($data = fgetcsv($handle, 10000, ",")) !== FALSE) {
        $num = count($data);
        if ( $num != $MY_COLS ){
          die("Ciselnik $csvfile ma nespravny pocet polozek $num, ocekavam $MY_COLS"); 
        }
//        echo "<p> $num fields in line $row: <br /></p>\n";
        $row++;
        
        $kod = $data[0];
        $nazev = $data[5];
        $JSON[] = array( 'id' => $kod, 'name' => nn($nazev));
    }
    fclose($handle);
    echo json_encode($JSON);
} else {
  die("fopen");
}

?>
