
set -ex
cd `dirname $0`
export LC_CTYPE=en_US.UTF-8
#[ -d ../build ] || mkdir ../build
{
echo -n "var diagData = "
piconv -f CP852 -t UTF-8 < JDG4.721 |
   php diag2json.php
echo ";"
} > data.js


